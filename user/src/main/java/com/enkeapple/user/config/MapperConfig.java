package com.enkeapple.user.config;

import com.enkeapple.user.mapper.UserPageMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {
    @Bean
    public UserPageMapper pageMapper() {
        return new UserPageMapper();
    }
}