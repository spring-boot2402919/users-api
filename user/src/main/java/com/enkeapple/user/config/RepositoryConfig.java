package com.enkeapple.user.config;

import com.enkeapple.user.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class RepositoryConfig {
    @Bean
    @Primary
    public UserRepository primaryUserRepository(UserRepository userRepository) {
        return userRepository;
    }
}
