package com.enkeapple.user.controller;

import com.enkeapple.common.controller.BaseController;
import com.enkeapple.user.domain.UserEntity;
import com.enkeapple.user.dto.PhoneDTO;
import com.enkeapple.user.dto.UserDTO;
import com.enkeapple.user.mapper.UserMapper;
import com.enkeapple.user.mapper.UserPageMapper;
import com.enkeapple.user.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/user")
@Tag(name = "Users")
@Validated
public class UserController extends BaseController<UserEntity, UserDTO> {
    final UserService userService;

    @Autowired
    protected UserController(UserMapper mapper, UserService service, UserPageMapper pageMapper, UserService userService) {
        super(service, mapper, pageMapper);

        this.userService = userService;
    }

    @PostMapping("/{id}/phone")
    public UserDTO createPhone(@PathVariable Long id, @RequestBody PhoneDTO phoneDTO) {
        return userService.addUserPhoneNumber(id, phoneDTO.getPhone());
    }
}
