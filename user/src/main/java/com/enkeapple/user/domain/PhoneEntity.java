package com.enkeapple.user.domain;

import com.enkeapple.common.domain.BaseEntity;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.DynamicUpdate;

@Entity
@Setter
@Getter
@DynamicUpdate
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "phones")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PhoneEntity extends BaseEntity {
    @Column
    String phone;
}