package com.enkeapple.user.repository;

import com.enkeapple.common.repository.BaseRepository;
import com.enkeapple.user.domain.UserEntity;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends BaseRepository<UserEntity> {}
