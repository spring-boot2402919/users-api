package com.enkeapple.user.repository;

import com.enkeapple.common.repository.BaseRepository;
import com.enkeapple.user.domain.PhoneEntity;

public interface PhoneRepository extends BaseRepository<PhoneEntity> {
    PhoneEntity findByPhone(String phone);
}
