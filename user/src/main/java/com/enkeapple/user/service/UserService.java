package com.enkeapple.user.service;

import com.enkeapple.common.service.BaseService;
import com.enkeapple.user.domain.PhoneEntity;
import com.enkeapple.user.domain.UserEntity;
import com.enkeapple.user.dto.UserDTO;
import com.enkeapple.user.exception.UserException;
import com.enkeapple.user.mapper.UserMapper;
import com.enkeapple.user.repository.PhoneRepository;
import com.enkeapple.user.repository.UserRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class UserService extends BaseService<UserEntity> implements IUserService {
    final UserRepository userRepository;
    final PhoneRepository phoneRepository;
    final UserMapper userMapper;

    @Autowired
    public UserService(
            @Qualifier("userRepository") UserRepository userRepository,
            @Qualifier("phoneRepository") PhoneRepository phoneRepository,
            UserMapper userMapper) {
        super(userRepository);

        this.userRepository = userRepository;
        this.phoneRepository = phoneRepository;
        this.userMapper = userMapper;
    }

    @Override
    public UserDTO addUserPhoneNumber(long id, String phoneNumber) {
        try {
            UserEntity user = super.findById(id);

            PhoneEntity existingPhone = phoneRepository.findByPhone(phoneNumber);

            if (existingPhone != null || user.getPhones().contains(null)) {
                throw new UserException("This phone number already use - " + phoneNumber, HttpStatus.CONFLICT);
            }

            PhoneEntity phone = PhoneEntity.builder()
                    .phone(phoneNumber)
                    .build();

            user.getPhones().add(phone);

            return userMapper.entityToDTO(userRepository.save(user));
        } catch (EntityNotFoundException e) {
            throw new UserException("User with ID " + id + " was not found in the system.", HttpStatus.NOT_FOUND);
        }
    }
}
