package com.enkeapple.user.service;

import com.enkeapple.user.dto.UserDTO;

public interface IUserService {
    UserDTO addUserPhoneNumber(long id, String phoneNumber);
}
