package com.enkeapple.user.mapper;

import com.enkeapple.common.mapper.IBasePageMapper;
import com.enkeapple.common.mapper.BasePageMapper;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
public class UserPageMapper implements IBasePageMapper {
    @Override
    public BasePageMapper convertPageToMap(Page page) {
        return BasePageMapper.builder()
                .total(page.getTotalElements())
                .page(page.getNumber())
                .size(page.getSize())
                .data(page.getContent())
                .build();
    }
}
