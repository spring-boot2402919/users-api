package com.enkeapple.user.mapper;

import com.enkeapple.common.mapper.IBaseMapper;
import com.enkeapple.user.domain.UserEntity;
import com.enkeapple.user.dto.UserDTO;
import org.mapstruct.*;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface UserMapper extends IBaseMapper<UserEntity, UserDTO> {
    @Override
    UserDTO entityToDTO(UserEntity entity);

    @Override
    UserEntity dtoToEntity(UserDTO dto);
}
