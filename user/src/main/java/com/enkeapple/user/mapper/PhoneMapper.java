package com.enkeapple.user.mapper;

import com.enkeapple.common.mapper.IBaseMapper;
import com.enkeapple.user.domain.PhoneEntity;
import com.enkeapple.user.dto.PhoneDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.ReportingPolicy;

@Mapper(
        componentModel = MappingConstants.ComponentModel.SPRING,
        unmappedTargetPolicy = ReportingPolicy.IGNORE
)
public interface PhoneMapper extends IBaseMapper<PhoneEntity, PhoneDTO> {
    @Override
    PhoneDTO entityToDTO(PhoneEntity entity);

    @Override
    PhoneEntity dtoToEntity(PhoneDTO dto);
}
