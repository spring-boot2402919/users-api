package com.enkeapple.user.dto;

import com.enkeapple.common.dto.BaseDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" })
public class PhoneDTO extends BaseDTO {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    long id;

    String phone;
}
