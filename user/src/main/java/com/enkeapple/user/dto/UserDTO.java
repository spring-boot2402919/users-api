package com.enkeapple.user.dto;

import com.enkeapple.common.dto.BaseDTO;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Validated
@JsonIgnoreProperties(value = { "createdAt", "updatedAt" })
public class UserDTO extends BaseDTO {
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    long id;

    String firstName;

    String lastName;

    @NotBlank(message = "Email is mandatory")
    String email;

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    List<PhoneDTO> phones;
}
