package com.enkeapple.common.service;

import com.enkeapple.common.domain.BaseEntity;
import com.enkeapple.common.repository.BaseRepository;
import jakarta.persistence.EntityNotFoundException;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@FieldDefaults(level = AccessLevel.PUBLIC)
public class BaseService<E extends BaseEntity> implements IService<E> {
    protected final BaseRepository<E> repository;

    @Autowired
    public BaseService(BaseRepository<E> repository) {
        this.repository = repository;
    }

    @Override
    @Transactional
    public E create(E entity) {
        return repository.save(entity);
    }

    @Override
    @Transactional
    public E update(E entity) {
        return repository.saveAndFlush(entity);
    }

    @Override
    public Page<E> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Override
    @Transactional(readOnly = true)
    public E findById(Long id) {
        Specification<E> specification = (root, query, cb) -> cb.equal(root.get("id"), id);

        return repository.findOne(specification)
                .orElseThrow(() -> new EntityNotFoundException("Entity with ID " + id + " was not found in the system."));
    }

    @Override
    public boolean delete(Long id) {
        repository.deleteById(id);

        return true;
    }
}
