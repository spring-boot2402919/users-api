package com.enkeapple.common.mapper;

import com.enkeapple.common.domain.BaseEntity;
import com.enkeapple.common.dto.BaseDTO;
import org.mapstruct.Mapping;

public interface IBaseMapper<E extends BaseEntity, D extends BaseDTO> {
    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    D entityToDTO(E entity);

    @Mapping(target = "createdAt", ignore = true)
    @Mapping(target = "updatedAt", ignore = true)
    E dtoToEntity(D dto);
}
