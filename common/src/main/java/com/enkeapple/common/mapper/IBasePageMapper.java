package com.enkeapple.common.mapper;

import org.springframework.data.domain.Page;

public interface IBasePageMapper {
    BasePageMapper convertPageToMap(Page page);
}
