package com.enkeapple.common.mapper;

import com.enkeapple.common.dto.BaseDTO;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import lombok.experimental.SuperBuilder;

import java.util.List;

@Data
@SuperBuilder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BasePageMapper<D extends BaseDTO> {
    long total;
    int size;
    int page;
    List<D> data;
}
