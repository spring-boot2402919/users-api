package com.enkeapple.common.controller;

import com.enkeapple.common.domain.BaseEntity;
import com.enkeapple.common.dto.BaseDTO;
import com.enkeapple.common.mapper.BasePageMapper;
import com.enkeapple.common.mapper.IBaseMapper;
import com.enkeapple.common.mapper.IBasePageMapper;
import com.enkeapple.common.service.BaseService;
import jakarta.validation.Valid;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Validated
@FieldDefaults(level = AccessLevel.PROTECTED)
public abstract class BaseController<E extends BaseEntity, D extends BaseDTO> {
    final BaseService<E> service;
    final IBaseMapper<E, D> mapper;
    final IBasePageMapper pageMapper;

    protected BaseController(BaseService<E> service, IBaseMapper<E, D> mapper, IBasePageMapper pageMapper) {
        this.service = service;
        this.mapper = mapper;
        this.pageMapper = pageMapper;
    }

    @GetMapping({"", ""})
    @Cacheable("users")
    public ResponseEntity<BasePageMapper<D>> getAll(
            @RequestParam(required = false, defaultValue = "0") Integer page,
            @RequestParam(required = false, defaultValue = "5") Integer size
    ) {
        Pageable pageable = PageRequest.of(page, size, Sort.by("id").ascending());

        Page<D> result = service.findAll(pageable).map(mapper::entityToDTO);

        BasePageMapper<D> response = pageMapper.convertPageToMap(result);

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    @Cacheable("users")
    public ResponseEntity<D> findById(@PathVariable long id) {
        E entity = service.findById(id);

        if (entity != null) {
            return ResponseEntity.ok(mapper.entityToDTO(entity));
        }

        return ResponseEntity.notFound().build();
    }

    @PostMapping({"", ""})
    public ResponseEntity<D> save(@Valid @RequestBody D body) {
        E entity = service.create(mapper.dtoToEntity(body));

        return ResponseEntity.status(HttpStatus.CREATED).body(mapper.entityToDTO(entity));
    }

    @DeleteMapping("/{id}")
    @CacheEvict("users")
    public ResponseEntity<String> delete(@PathVariable long id) {
        boolean deleted = service.delete(id);

        if (deleted) {
            return new ResponseEntity<>("Object with ID " + id + " has been successfully deleted.", HttpStatus.OK);
        }

        return new ResponseEntity<>(HttpStatus.NOT_FOUND.getReasonPhrase(), HttpStatus.NOT_FOUND);
    }

    @PutMapping("/{id}")
    @CachePut("users")
    public ResponseEntity<String> update(@PathVariable long id, @Valid @RequestBody D body) {
        E entity = mapper.dtoToEntity(body);

        entity.setId(id);

        service.update(entity);

        return new ResponseEntity<>("Object with ID " + id + " has been successfully updated.", HttpStatus.OK);
    }
}
